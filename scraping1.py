import urllib.request,sys,time
from bs4 import BeautifulSoup
import requests
import pandas as pd
from datetime import date
from datetime import timedelta
from datetime import datetime

now = datetime.now()
print('Current DateTime:', now)
current_date = now.date()
year= now.year
month=now.month
day=now.day
today = date(year, month, day)

prevdate=today - timedelta(days=7)
x=str(prevdate).split("-")
year=x[0]
month=x[1]
day=x[2]
pageinc=1
upperframe=[]  
for page in range(1,pageinc+1):
    
    url = 'https://www.prnewswire.com/news-releases/news-releases-list/?month='+str(month)+'&day='+str(day)+'&year='+str(year)+'&hour=12'
    print(url)
    
    try:
        
        page=requests.get(url)                            
    
    except Exception as e:
        print(e)
        continue                                              
    time.sleep(2)   
    soup=BeautifulSoup(page.text,'html.parser')
    #print(soup)
    
    
    
   
    frame=[]
    links=soup.find_all('div',attrs={'class':'row arabiclistingcards'})
    print("The links")
    print(links)
    Times=soup.find_all('div',attrs={'class':'col-sm-8 col-lg-9 pull-left card'})
    print(Times)
    
    for tags in soup.find_all("h3"):
        print(tags.name + ' -> ' + tags.text.strip())
        
    filename="NEWS.csv"
    f=open(filename,"w", encoding = 'utf-8')
    headers="Statement,Link,Date, Source, Label\n"
    f.write(headers)
    
    for j in links:
        
        Link = "https://www.prnewswire.com/"
        Link += j.find("div",attrs={'class':'card col-view'}).find('a')['href'].strip()
        print(Link)
        exit()
        statement=j.find("div",attrs={'class':'row detail-headline'})
        Date = j.find('div',attrs={'class':'m-statement__body'}).find('footer').text[-14:-1].strip()
        Source = j.find('div', attrs={'class':'m-statement__meta'}).find('a').text.strip()
        Label = j.find('div', attrs ={'class':'m-statement__content'}).find('img',attrs={'class':'c-image__original'}).get('alt').strip()
        frame.append((Statement,Link,Date,Source,Label))
        f.write(Statement.replace(",","^")+","+Link+","+Date.replace(",","^")+","+Source.replace(",","^")+","+Label.replace(",","^")+"\n")
    upperframe.extend(frame)
f.close()
data=pd.DataFrame(upperframe, columns=['Statement','Link','Date','Source','Label'])
data.head()
